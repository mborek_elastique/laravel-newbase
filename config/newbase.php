<?php

use Illuminate\Database\Eloquent\Model;
use Wocozon\Newbase\Client\Resources\Project;
use Wocozon\Newbase\Client\Resources\User;

return [
    'apiUrl' => 'https://webservice.newbase.nl/servoy-service/rest_ws/newbase_ws/',
    'apiToken' => \env('NEWBASE_TOKEN', null),
    'client' => [
        'timeout' => 60,
        'allow_redirects' => [
            'max' => 10,
            'strict' => true,
            'referer' => true,
            'protocols' => ['https'],
            'track_redirects' => true,
        ],
        'connect_timeout' => 10,
        'read_timeout' => 10,
        'verify' => false,
        'debug' => \env('APP_DEBUG', false),
    ],
    'credentials' => [
        'ownerId' => \env('NEWBASE_OWNER_ID', null),
        'companyId' => \env('NEWBASE_COMPANY_ID', null),
        'userId' => \env('NEWBASE_USER_ID', null),
        'username' => \env('NEWBASE_USERNAME', null),
        'password' => \env('NEWBASE_PASSWORD', null),
    ],
    'mapping' => [
        \Illuminate\Foundation\Auth\User::class => User::class,
        Model::class => Project::class,
    ],
    'observe' => [],
];
