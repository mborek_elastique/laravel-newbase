# Laravel Newbase

Put in your `.env` file following variables:

    NEWBASE_OWNER_ID=
    NEWBASE_COMPANY_ID=
    NEWBASE_USER_ID=
    NEWBASE_USERNAME=
    NEWBASE_PASSWORD=
    NEWBASE_COMPANY_CODE=
    
Publish your config file with `vendor:publish` Artisan command.
Adjust `config/newbase.php` file to make it fit your needs - override `mapping` and `observe` sections.
