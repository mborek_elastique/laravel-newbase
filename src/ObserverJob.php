<?php

namespace Wocozon\Newbase;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Wocozon\Newbase\Contracts\NewbaseSerializableInterface;

abstract class ObserverJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var Model|NewbaseSerializableInterface
     */
    protected Model $model;

    /**
     * ObserverJob constructor.
     * @param Model|NewbaseSerializableInterface $model
     */
    public function __construct(Model $model)
    {
        if (($model instanceof NewbaseSerializableInterface) === false) {
            throw new Exception("{$model} has no ObservableInterface implementation!");
        }

        $this->model = $model;
    }

    /**
     *
     */
    abstract public function handle(): void;
}
