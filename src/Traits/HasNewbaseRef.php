<?php

namespace Wocozon\Newbase\Traits;

use Illuminate\Database\Eloquent\Model;
use Wocozon\Newbase\Client\Contracts\ReferenceInterface;
use Wocozon\Newbase\Contracts\NewbaseRefInterface;
use Wocozon\Newbase\ResourceFactory;

/**
 * Trait HasNewbaseRef
 * @package Wocozon\Newbase\Traits
 *
 * @mixin Model
 * @mixin NewbaseRefInterface
 */
trait HasNewbaseRef
{
    public function getNewbaseByRef(): ?array
    {
        $resource = ResourceFactory::resolveModel($this);
        if ($this instanceof NewbaseRefInterface
            && $resource instanceof ReferenceInterface
            && $this->getNewbaseRefField()) {
            return $resource->findByRef($this->getNewbaseRefValue());
        }

        return null;
    }

    public function getNewbaseRefValue(): ?string
    {
        return $this->getAttributeValue($this->getNewbaseRefField());
    }

    public function getNewbaseRefField(): ?string
    {
        return $this->nbRefName ?? null;
    }
}
