<?php

namespace Wocozon\Newbase\Traits;

use Illuminate\Database\Eloquent\Model;
use Wocozon\Newbase\Contracts\NewbaseUuidInterface;
use Wocozon\Newbase\Exception;
use Wocozon\Newbase\ModelObserver;
use Wocozon\Newbase\ResourceFactory;

/**
 * Trait HasNewbaseUuid
 * @package Wocozon\Newbase\Traits
 *
 * @mixin Model
 * @mixin NewbaseUuidInterface
 */
trait HasNewbaseUuid
{
    public function getNewbaseByUuid(): array
    {
        $uuid = $this->getNewbaseUuidValue();
        if (!$uuid) {
            throw new Exception("{$this} has no Newbase UUID!");
        }
        $resource = ResourceFactory::resolveModel($this);
        return $resource->findByPK($uuid);
    }

    public function getNewbaseUuidValue(): ?string
    {
        return $this->getAttributeValue($this->getNewbaseUuidField());
    }

    public function getNewbaseUuidField(): string
    {
        return $this->nbFieldName ?? ModelObserver::DEFAULT_UUID_FIELD;
    }
}
