<?php

namespace Wocozon\Newbase\Traits;

use Illuminate\Database\Eloquent\Model;
use Wocozon\Newbase\Client\Resources\Resource;
use Wocozon\Newbase\ResourceFactory;

/**
 * Trait HasNewbaseResource
 * @package Wocozon\Newbase\Traits
 *
 * @mixin Model
 */
trait HasNewbaseResource
{
    public function getNewbaseResource(): Resource
    {
        return ResourceFactory::resolveModel($this);
    }
}
