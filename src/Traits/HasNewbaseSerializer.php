<?php

namespace Wocozon\Newbase\Traits;

use Illuminate\Database\Eloquent\Model;
use Wocozon\Newbase\Contracts\NewbaseSerializableInterface;

/**
 * Trait HasNewbaseSerializer
 * @package Wocozon\Newbase\Traits
 *
 * @mixin Model
 * @mixin NewbaseSerializableInterface
 */
trait HasNewbaseSerializer
{
    public function toNewbase(): array
    {
        return $this->attributesToArray();
    }
}
