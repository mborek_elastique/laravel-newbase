<?php

namespace Wocozon\Newbase\Jobs;

use Illuminate\Database\Eloquent\Model;
use Wocozon\Newbase\Client\Contracts\ModelResourceInterface;
use Wocozon\Newbase\Client\Contracts\ReferenceInterface;
use Wocozon\Newbase\Contracts\NewbaseRefInterface;
use Wocozon\Newbase\Contracts\NewbaseSerializableInterface;
use Wocozon\Newbase\Contracts\NewbaseUuidInterface;
use Wocozon\Newbase\Exception;
use Wocozon\Newbase\ObserverJob;
use Wocozon\Newbase\ResourceFactory;

class ObservableCreated extends ObserverJob
{
    public function handle(): void
    {
        $resource = ResourceFactory::resolveModel($this->model);

        if ($resource instanceof ModelResourceInterface
            && $this->model instanceof NewbaseSerializableInterface
            && $this->model instanceof NewbaseUuidInterface) {
            $status = $resource->create($this->model->toNewbase());
            if (!$status) {
                throw new Exception('Model was not created!');
            }
            //dump([$this->model, $resource, $status]);
            if ($this->model instanceof NewbaseUuidInterface
                && $resource instanceof ReferenceInterface
                && $status[$resource->getPKColName()] ?? false) {
                $this->model->setAttribute($this->model->getNewbaseUuidField(), $status[$resource->getPKColName()]);
            } elseif ($this->model instanceof NewbaseRefInterface
                && $resource instanceof ReferenceInterface
                && $status[$resource->getRefColName()] ?? false) {
                $result = $this->model->getNewbaseByRef();
                if ($result) {
                    $this->model->setAttribute($this->model->getNewbaseUuidField(), $result[$resource->getPKColName()]);
                }
            }
            Model::withoutEvents(function () {
                $this->model->save();
            });
        }
    }
}
