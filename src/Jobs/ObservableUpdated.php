<?php

namespace Wocozon\Newbase\Jobs;

use Wocozon\Newbase\Client\Contracts\ModelResourceInterface;
use Wocozon\Newbase\Contracts\NewbaseSerializableInterface;
use Wocozon\Newbase\Contracts\NewbaseUuidInterface;
use Wocozon\Newbase\Exception;
use Wocozon\Newbase\ObserverJob;
use Wocozon\Newbase\ResourceFactory;

class ObservableUpdated extends ObserverJob
{
    public function handle(): void
    {
        $resource = ResourceFactory::resolveModel($this->model);

        if ($resource instanceof ModelResourceInterface
            && $this->model instanceof NewbaseSerializableInterface
            && $this->model instanceof NewbaseUuidInterface) {
            $status = $resource->update(
                [$resource->getPKColName() => $this->model->getNewbaseUuidValue()],
                $this->model->toNewbase()
            );
            if (!$status) {
                throw new Exception('Model was not updated!');
            }
            //dump([$this->model, $resource, $status]);
        }
    }
}
