<?php

namespace Wocozon\Newbase;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Wocozon\Newbase\Client\Resources\Resource;
use Wocozon\Newbase\Exceptions\ResourceFactoryException;

/**
 * Class ResourceFactory
 * @package Wocozon\Newbase
 */
class ResourceFactory
{
    /**
     * @param Model $model
     * @return Resource
     */
    public static function resolveModel(Model $model): Resource
    {
        $mapping = \config('newbase.mapping', []);
        $reflection = new \ReflectionObject($model);
        $table = Str::lower(Str::snake($reflection->getShortName()));
        foreach ($mapping as $modelClass => $resourceClass) {
            if ($model instanceof $modelClass) {
                if ($resourceClass === Resource::class) {
                    return \resolve($resourceClass, compact('table'));
                }
                return \resolve($resourceClass);
            }
        }

        throw new ResourceFactoryException('No valid mapping!');
    }

    /**
     * @param string $class
     * @return Resource
     * @throws \ReflectionException
     */
    public static function resolveClass(string $class): Resource
    {
        $mapping = \config('newbase.mapping', []);
        $reflection = new \ReflectionClass($class);
        $table = Str::lower(Str::snake($reflection->getShortName()));
        foreach ($mapping as $modelClass => $resourceClass) {
            if ($class === $modelClass) {
                if ($resourceClass === Resource::class) {
                    return \resolve($resourceClass, compact('table'));
                }
                return \resolve($resourceClass);
            }
        }

        throw new ResourceFactoryException('No valid mapping!');
    }
}
