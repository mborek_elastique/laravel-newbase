<?php

namespace Wocozon\Newbase\Contracts;

/**
 * Interface NewbaseRefInterface
 * @package Wocozon\Newbase\Contracts
 */
interface NewbaseRefInterface
{
    /**
     * @return array|null
     */
    public function getNewbaseByRef(): ?array;

    /**
     * @return string|null
     */
    public function getNewbaseRefField(): ?string;

    /**
     * @return string|null
     */
    public function getNewbaseRefValue(): ?string;
}
