<?php

namespace Wocozon\Newbase\Contracts;

/**
 * Interface NewbaseSerializableInterface
 * @package Wocozon\Newbase\Contracts
 */
interface NewbaseSerializableInterface
{
    /**
     * @return array
     */
    public function toNewbase(): array;
}
