<?php

namespace Wocozon\Newbase\Contracts;

/**
 * Interface NewbaseUuidInterface
 * @package Wocozon\Newbase\Contracts
 */
interface NewbaseUuidInterface
{
    /**
     * @return array
     */
    public function getNewbaseByUuid(): array;

    /**
     * @return string
     */
    public function getNewbaseUuidField(): string;

    /**
     * @return string|null
     */
    public function getNewbaseUuidValue(): ?string;
}
