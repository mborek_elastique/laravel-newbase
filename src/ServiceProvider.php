<?php

namespace Wocozon\Newbase;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

//use Wocozon\Newbase\Client\Newbase;

/**
 * Class ServiceProvider
 * @package Wocozon\Newbase
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     *
     */
    public function boot(): void
    {
        $observables = \config('newbase.observe', []);
        //dump($observables);
        foreach ($observables as $key => $value) {
            if (\is_numeric($key) && (new \ReflectionClass($value))->hasMethod('observe')) {
                $value::observe(ModelObserver::class);
            } elseif (\is_string($key) && \is_string($value) && (new \ReflectionClass($key))->hasMethod('observe') && (new \ReflectionClass($value))->isInstantiable()) {
                $key::observe($value);
            }
        }

        if ($this->app->runningInConsole()) {
            $this->publishes(
                [
                    \dirname(__DIR__) . '/config/newbase.php' => \config_path('newbase.php'),
                ]
            );
        }
    }

    /**
     *
     */
    public function register(): void
    {
        $this->mergeConfigFrom(
            \dirname(__DIR__) . '/config/newbase.php',
            'newbase'
        );
    }
}
