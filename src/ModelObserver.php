<?php

namespace Wocozon\Newbase;

use Illuminate\Database\Eloquent\Model;
use Wocozon\Newbase\Contracts\NewbaseSerializableInterface;
use Wocozon\Newbase\Contracts\NewbaseUuidInterface;
use Wocozon\Newbase\Jobs\ObservableCreated;
use Wocozon\Newbase\Jobs\ObservableUpdated;

class ModelObserver
{
    /** @var string */
    public const DEFAULT_UUID_FIELD = 'newbase_uuid';

    public function created(Model $object): void
    {
        if ($object instanceof NewbaseSerializableInterface
            && $object instanceof NewbaseUuidInterface
            && !$object->getNewbaseUuidValue()) {
            ObservableCreated::dispatch($object)->onQueue('newbase'); // TODO: configurable
        }
    }

    public function updated(Model $object): void
    {
        if ($object instanceof NewbaseSerializableInterface
            && $object instanceof NewbaseUuidInterface
            && $object->getNewbaseUuidValue()) {
            ObservableUpdated::dispatch($object)->onQueue('newbase'); // TODO: configurable
        }
    }

    public function deleted(Model $object): void
    {
        //
        //\logger("{$object} deleted!");
    }
}
