<?php

namespace Wocozon\Newbase\Client\Contracts;

use Wocozon\Newbase\Client\Query\Builder;
use Wocozon\Newbase\Client\Query\Result;

interface ResourceInterface
{
    public function getPKColName(): string;

    public function makeQuery(): Builder;

    public function executeQuery(Builder $builder): Result;

    public function findByPK(string $id): ?array;
}
