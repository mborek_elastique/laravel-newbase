<?php

namespace Wocozon\Newbase\Client\Contracts;

interface ModelResourceInterface extends ResourceInterface
{
    public function create(array $object): ?array;

    public function read(array $keys): ?array;

    public function update(array $keys, array $object): bool;

    public function delete(array $keys): bool;
}
