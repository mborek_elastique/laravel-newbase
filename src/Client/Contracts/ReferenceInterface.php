<?php

namespace Wocozon\Newbase\Client\Contracts;

interface ReferenceInterface extends ResourceInterface
{
    public function getRefColName(): string;

    public function findByRef(string $ref): array;
}
