<?php

namespace Wocozon\Newbase\Client\Query;

use Illuminate\Support\Collection;

class Result
{
    protected Collection $results;
    protected array $attributes;

    public function __construct(Collection $results, ?array $attributes = null)
    {
        $this->results = $results;
        $this->attributes = $attributes ?? [];
    }

    /**
     * @return Collection
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }
}
