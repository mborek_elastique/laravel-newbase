<?php

namespace Wocozon\Newbase\Client\Query;

use Wocozon\Newbase\Client\Exception;

/**
 * Class QueryBuilder
 * @package Wocozon\Newbase\Client
 */
class Builder
{
    /** @var array */
    protected array $statement = [];

    /** @var array */
    protected array $columns = [];

    /** @var array */
    protected array $joins = [];

    /** @var array */
    protected array $order = [];

    /**
     * @param string $column
     * @param string $operator
     * @param $value
     * @param string|null $subcondition
     * @return $this
     */
    public function addStatement(string $column, string $operator, $value, ?string $subcondition = null): self
    {
        $this->statement[] = \compact('column', 'operator', 'subcondition', 'value');

        return $this;
    }

    /**
     * @param string $table
     * @param string|null $name
     * @return $this
     */
    public function addColumn(string $table, ?string $name = null): self
    {
        if (!\is_array($this->columns[$table] ?? false)) {
            $this->columns[$table] = null;
        }

        if ($name) {
            $this->columns[$table] = \array_merge(
                $this->columns[$table],
                [
                    $name => true,
                ]
            );
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function addJoin(): self
    {
        return $this;
    }

    /**
     * @param string $field
     * @param string $order
     * @return $this
     */
    public function addOrder(string $field, string $order = 'desc'): self
    {
        $this->order[$field] = \strtoupper($order);

        return $this;
    }

    /**
     * @return string
     */
    public function build(): string
    {
        $columns = [];

        foreach (\array_keys($this->columns) as $table) {
            $columns[] = [
                $table => $this->columns[$table] ?? null,
            ];
        }

        $query = [
            [
                'statement' => [
                    [
                        'AND' => $this->statement,
                    ],
                ],
                'columns' => $columns,
                'order' => $this->order,
                'joins' => $this->joins,
            ],
        ];

        $raw = $this->encodeQuery($query);

        if ($raw !== null) {
            return $raw;
        }

        throw new Exception('Query cannot be encoded correctly!');
    }

    /**
     * @param array $query
     * @return string|null
     */
    private function encodeQuery(array $query): ?string
    {
        try {
            $text = \json_encode($query, JSON_THROW_ON_ERROR);
        } catch (\JsonException $exception) {
            return null;
        }

        while (\preg_match('/(.+)(")(\w+)(")(\:)(.+)/', $text, $matches) === 1) {
            unset($matches[0]);
            unset($matches[2]);
            unset($matches[4]);
            $text = implode('', $matches);
        }

        return \str_replace('"', '\'', $text);
    }
}
