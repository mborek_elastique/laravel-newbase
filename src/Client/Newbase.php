<?php

namespace Wocozon\Newbase\Client;

use Carbon\CarbonInterval;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Wocozon\Newbase\Client\Resources\Auth;

/**
 * Class Newbase
 * @package Wocozon\Newbase\Client
 */
class Newbase
{
    /** @var int */
    public const API_VERSION = 4;
    /** @var int */
    public const DEFAULT_QUERY_LIMIT = 1;
    /** @var string|null */
    private ?string $token = null;
    /** @var Client */
    private Client $client;

    /**
     * Newbase constructor.
     * @param string|null $token
     */
    public function __construct(string $token = null)
    {
        $this->client = new Client(
            \array_merge(
                [
                    'timeout' => 60,
                    'connect_timeout' => 10,
                    'read_timeout' => 10,
                    'verify' => false,
                    'debug' => true,
                ],
                \config('newbase.client'),
                [
                    'base_uri' => \config('newbase.apiUrl'),
                ]
            )
        );

        $this->token = $token ?? $this->getToken();
    }

    /**
     * @return Client
     */
    public function getHttpClient(): Client
    {
        return $this->client;
    }

    /**
     * @param array $input
     * @return array
     */
    public function applyCredentials(array $input): array
    {
        return \array_merge(
            $input,
            [
                'companyId' => \config('newbase.credentials.companyId'),
                'userId' => \config('newbase.credentials.userId'),
                'ownerId' => \config('newbase.credentials.ownerId'),
            ]
        );
    }

    /**
     * @param array $input
     * @return array
     */
    public function applyToken(array $input): array
    {
        return \array_merge(
            $input,
            [
                'token' => $this->token,
            ]
        );
    }

    public function getToken(): string
    {
        if ($this->token) {
            return $this->token;
        }

        if (\config('newbase.apiToken', null) !== null) {
            return \config('newbase.apiToken');
        }

        $ttl = CarbonInterval::day();
        return Cache::remember('Newbase-ApiClientToken', $ttl, function () use ($ttl) {
            return (new Auth($this))->createNewToken($ttl);
        });
    }
}
