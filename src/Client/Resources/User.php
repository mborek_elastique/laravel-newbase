<?php

namespace Wocozon\Newbase\Client\Resources;

use Wocozon\Newbase\Client\Contracts\ReferenceInterface;
use Wocozon\Newbase\Client\Resources\Traits\HasReferenceField;

/**
 * Class User
 * @package Wocozon\Newbase\Client\Resources
 */
class User extends Resource implements ReferenceInterface
{
    use HasReferenceField;

    /** @var array|string[] */
    protected array $tables = [
        'app_user',
    ];

    /** @var string */
    protected string $refField = 'email_address';

    /**
     * @param string $mail
     * @return array|null
     */
    public function findByMail(string $mail): ?array
    {
        return $this->read(['email_address' => $mail]);
    }
}
