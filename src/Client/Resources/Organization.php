<?php

namespace Wocozon\Newbase\Client\Resources;

use Wocozon\Newbase\Client\Resources\Traits\HasReferenceField;

class Organization extends Resource
{
    use HasReferenceField;

    /** @var string */
    protected string $refField = 'serial';

    /** @var array|string[] */
    protected array $tables = [
        'organisation',
    ];
}
