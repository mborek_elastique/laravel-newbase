<?php

namespace Wocozon\Newbase\Client\Resources;

use Wocozon\Newbase\Client\Contracts\ModelResourceInterface;
use Wocozon\Newbase\Client\Resources\Traits\HasReadModelOperation;
use Wocozon\Newbase\Client\Resources\Traits\HasWriteModelOperations;

class AccountManager extends Resource implements ModelResourceInterface
{
    use HasReadModelOperation;
    use HasWriteModelOperations;

    /**
     * @var int
     */
    protected int $perPage = 10;
}
