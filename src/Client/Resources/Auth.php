<?php

namespace Wocozon\Newbase\Client\Resources;

use Carbon\CarbonInterval;
use GuzzleHttp\Exception\RequestException;
use Wocozon\Newbase\Client\Exception;

/**
 * Class Auth
 * @package Wocozon\Newbase\Client\Resources
 */
class Auth extends Resource
{
    /**
     * @return string
     * @throws \JsonException
     */
    public function createNewToken(?CarbonInterval $ttl = null): string
    {
        $ttl = $ttl ?? CarbonInterval::day();

        $query = [
            'username' => \config('newbase.credentials.username'),
            'password' => \config('newbase.credentials.password'),
            'expire_date' => \now()->add($ttl)->format('Y-m-d'),
            'application_name' => sprintf('%s-APIClient', \config('app.name', 'Newbase')),
        ];

        $query = $this->client->applyCredentials($query);

        try {
            $response = $this->client->getHttpClient()->post(
                'ws_check_token',
                [
                    'json' => $query,
                ]
            );
        } catch (RequestException $exception) {
            throw new Exception('Request failed!', 0, $exception);
        }

        try {
            $data = \json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $exception) {
            throw new Exception('Cannot dispatch response!', 0, $exception);
        }

        if (($data['success'] ?? null) === true && ($data['token'] ?? null) !== null) {
            return $data['token'];
        }

        if (($data['success'] ?? null) === false && ($data['message'] ?? null) !== null) {
            throw new Exception("Newbase failure: {$data['message']}", $response->getStatusCode());
        }

        throw new Exception('Token cannot be fetched!');
    }
}
