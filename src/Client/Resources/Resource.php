<?php

namespace Wocozon\Newbase\Client\Resources;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Wocozon\Newbase\Client\Contracts\ResourceInterface;
use Wocozon\Newbase\Client\Newbase;
use Wocozon\Newbase\Client\Query\Builder;
use Wocozon\Newbase\Client\Resources\Traits\HasReadModelOperation;
use Wocozon\Newbase\Client\Resources\Traits\UseQueryBuilder;

/**
 * Class Resource
 * @package Wocozon\Newbase\Client\Resources
 */
class Resource implements ResourceInterface
{
    use UseQueryBuilder;
    use HasReadModelOperation;

    /** @var Newbase */
    protected Newbase $client;

    /** @var string */
    protected string $uri;

    /** @var int */
    protected int $perPage;

    /** @var array */
    protected array $tables = [];

    /** @var string */
    protected string $primaryKey;

    /** @var array|null */
    protected ?array $columns = null;

    /**
     * Resource constructor.
     * @param Newbase $client
     * @param string|null $uri
     * @param string|null $primaryKey
     * @param string|null $table
     * @param int|null $perPage
     */
    public function __construct(
        Newbase $client,
        ?string $table = null,
        ?string $uri = null,
        ?string $primaryKey = null,
        ?int $perPage = null
    ) {
        $this->client = $client;

        if (empty($this->tables)) {
            $this->tables[] = $table ?? $this->getDefaultTable();
        }

        $this->uri = $uri ?? $this->getUri();
        $this->primaryKey = $primaryKey ?? $this->getPKColName();
        $this->perPage = $perPage ?? $this->perPage ?? Newbase::DEFAULT_QUERY_LIMIT;
    }

    /**
     * @return string
     */
    public function getDefaultTable(): string
    {
        return $this->tables[0] ?? $this->getModelName();
    }

    /**
     * @return string
     */
    protected function getModelName(): string
    {
        return Str::lower(Str::snake((new \ReflectionObject($this))->getShortName()));
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri ?? "ws_{$this->getDefaultTable()}";
    }

    /**
     * @return string
     */
    public function getPKColName(): string
    {
        return $this->primaryKey ?? "k_{$this->getDefaultTable()}_id";
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        $builder = $this->makeQuery();

        $page = 0;
        $collection = \collect();
        do {
            $page++;
            $result = $this->executeQuery($builder, $page);
            $collection = $collection->merge($result->getResults());
        } while ($result->getAttributes()['last_page'] > $page);

        return $collection;
    }

    /**
     * @return Builder
     */
    public function makeQuery(): Builder
    {
        $builder = new Builder();
        if ($this->columns === null) {
            $builder->addColumn($this->getDefaultTable());
        } else {
            foreach ($this->columns as $column) {
                $builder->addColumn($this->getDefaultTable(), $column);
            }
        }
        return $builder;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        $builder = $this->makeQuery();
        $result = $this->executeQuery($builder, 1, 1);
        return $result->getAttributes()['total_records'] ?? 0;
    }

    /**
     * @param string $id
     * @return array
     */
    public function findByPK(string $id): ?array
    {
        return $this->read([$this->getPKColName() => $id]);
    }
}
