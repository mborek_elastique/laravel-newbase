<?php

namespace Wocozon\Newbase\Client\Resources;

use Wocozon\Newbase\Client\Resources\Traits\HasReferenceField;

class Debtor extends Resource
{
    use HasReferenceField;

    /** @var string */
    protected string $refField = 'serial';
}
