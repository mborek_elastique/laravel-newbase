<?php


namespace Wocozon\Newbase\Client\Resources\Traits;

use GuzzleHttp\Exception\RequestException;
use Wocozon\Newbase\Client\Exception;
use Wocozon\Newbase\Client\Resources\Resource;

/**
 * Trait HasWriteModelOperations
 * @package Wocozon\Newbase\Client\Resources\Traits
 *
 * @mixin Resource
 */
trait HasWriteModelOperations
{
    /**
     * @param array $object
     * @return string|null
     */
    public function create(array $object): ?array
    {
        $query = $object;
        $query = $this->client->applyToken($query);
        $query = $this->client->applyCredentials($query);
        $query = $this->applyModelQuery($query);

        $query['lock_record'] = true;

        try {
            $response = $this->client->getHttpClient()->post(
                $this->uri,
                [
                    'json' => $query,
                ]
            );
        } catch (RequestException $exception) {
            throw new Exception('Request failed!', 0, $exception);
        }

        try {
            $data = \json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $exception) {
            throw new Exception('Cannot dispatch response!', 0, $exception);
        }

        if (\config('app.debug')) {
            \logger("Newbase API create model request", $query);
            \logger("Newbase API create model response", $data);
        }

        if (($response->getStatusCode() === 200 && ($data['success'] ?? null) === true) && ($data['result'] ?? null) !== null) {
            return $data['result'];
        }

        if (($data['message'] ?? null) !== null && ($response->getStatusCode() !== 200 || ($data['success'] ?? null) === false)) {
            throw new Exception("Newbase failure: {$data['message']}", $response->getStatusCode());
        }

        throw new Exception('Unexpected response from Newbase!');
    }

    /**
     * @param array $keys
     * @param array $object
     * @return bool
     */
    public function update(array $keys, array $object): bool
    {
        $query = $this->client->applyToken([]);
        $query = $this->client->applyCredentials($query);
        $query['data'] = $this->applyModelQuery(\array_merge($keys, $object));

        try {
            $response = $this->client->getHttpClient()->put(
                $this->uri,
                [
                    'json' => $query,
                ]
            );
        } catch (RequestException $exception) {
            throw new Exception('Request failed!', 0, $exception);
        }

        try {
            $data = \json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $exception) {
            throw new Exception('Cannot dispatch response!', 0, $exception);
        }

        if (\config('app.debug')) {
            \logger("Newbase API update model request", $query);
            \logger("Newbase API update model response", $data);
        }

        if (($data['success'] ?? null) === true) {
            return true;
        }

        if (($data['success'] ?? null) === false && ($data['message'] ?? null) !== null) {
            throw new Exception("Newbase failure: {$data['message']}", $response->getStatusCode());
        }

        throw new Exception('Unexpected response from Newbase!');
    }

    /**
     * @param array $keys
     * @return bool
     */
    public function delete(array $keys): bool
    {
        return false;
    }

    public function applyModelQuery(array $query): array
    {
        return \array_merge([
            'method' => $this->getUri(),
        ], $query);
    }
}
