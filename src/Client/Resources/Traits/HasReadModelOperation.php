<?php


namespace Wocozon\Newbase\Client\Resources\Traits;

//use Wocozon\Newbase\Client\Exception;
use Wocozon\Newbase\Client\Exception;
use Wocozon\Newbase\Client\Resources\Resource;

/**
 * Class HasReadModelOperation
 * @package Wocozon\Newbase\Client\Resources\Traits
 *
 * @mixin Resource
 */
trait HasReadModelOperation
{
    /**
     * @param array $keys
     * @return array|null
     */
    public function read(array $keys): ?array
    {
        $builder = $this->makeQuery();

        foreach ($keys as $field => $value) {
            $builder->addStatement($field, 'eq', $value);
        }

        $result = $this->executeQuery($builder, 1, 1)->getResults()->first();

        if (!$result) {
            throw new Exception('Model for given criteria does not exist!');
        }

        return $result;
    }
}
