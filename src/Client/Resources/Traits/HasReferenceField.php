<?php

namespace Wocozon\Newbase\Client\Resources\Traits;

use Wocozon\Newbase\Client\Resources\Resource;

/**
 * Trait HasReferenceField
 * @package Wocozon\Newbase\Client\Resources\Traits
 *
 * @mixin Resource
 */
trait HasReferenceField
{
    public function findByRef(string $ref): array
    {
        return $this->read([$this->getRefColName() => $ref]);
    }

    public function getRefColName(): string
    {
        return $this->refField ?? 'serial';
    }
}
