<?php

namespace Wocozon\Newbase\Client\Resources\Traits;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Collection;
use Wocozon\Newbase\Client\Exception;
use Wocozon\Newbase\Client\Newbase;
use Wocozon\Newbase\Client\Query\Builder;
use Wocozon\Newbase\Client\Query\Result;
use Wocozon\Newbase\Client\Resources\Resource;

/**
 * Trait UseQueryBuilder
 * @package Wocozon\Newbase\Client\Resources\Traits
 *
 * @mixin Resource
 *
 * @property $perPage int|null
 * @property $client Newbase
 */
trait UseQueryBuilder
{
    /**
     * @param Builder $builder
     * @param int $page
     * @param int|null $limit
     * @return Result
     */
    public function executeQuery(Builder $builder, int $page = 1, ?int $limit = null): Result
    {
        $limit = $limit ?? $this->getLimit();

        $query = \compact('page', 'limit');
        $query = $this->client->applyCredentials($query);
        $query = $this->client->applyToken($query);
        $query = $this->applyQueryBuilder(
            $query,
            $this->getDefaultTable(),
            $this->getDefaultTable(),
            $builder
        );

        try {
            $response = $this->client->getHttpClient()->get(
                'ws_query_builder',
                [
                    'query' => $query,
                ]
            );
        } catch (RequestException $exception) {
            \logger("Newbase API execute query request", $query);
            throw new Exception("Request failed: {$exception->getMessage()}!"); //, 0, $exception);
        }

        try {
            $data = \json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $exception) {
            throw new Exception('Cannot dispatch response!', 0, $exception);
        }

        if (\config('app.debug')) {
            \logger("Newbase API execute query request", $query);
            \logger("Newbase API execute query response", $data);
        }

        if (($data['success'] ?? null) === false && $data['message'] ?? null) {
            throw new Exception("Newbase failure: {$data['message']}", $response->getStatusCode());
        }

        if (($data['success'] ?? null) === true) {
            return $this->prepareResult($data);
        }

        throw new Exception('Cannot parse results!', $response->getStatusCode());
    }

    /**
     * @return int
     */
    protected function getLimit(): int
    {
        return $this->perPage ?? Newbase::DEFAULT_QUERY_LIMIT;
    }

    /**
     * @param array $input
     * @param string $program
     * @param string $queryTable
     * @param Builder $query
     * @param array $options
     * @return array
     */
    protected function applyQueryBuilder(
        array $input,
        string $program,
        string $queryTable,
        Builder $query,
        array $options = []
    ): array {
        //\logger('Newbase API query param', [\urlencode($query->build())]);
        return \array_merge(
            $input,
            [
                'v' => Newbase::API_VERSION,
                'method' => 'query_builder',
                'application_name' => 'newbase_query_builder',
                'no_media' => true,
                'convert_date' => true,
                'query' => $query->build(),
                'program' => $program,
                'query_table' => $queryTable,
            ],
            $options,
        );
    }

    /**
     * @param array $data
     * @return Result
     */
    private function prepareResult(array $data): Result
    {
        return new Result($this->prepareList($data), $this->prepareAttributes($data));
    }

    /**
     * @param array $data
     * @return Collection
     */
    private function prepareList(array $data): Collection
    {
        return \collect($data['data'][$this->uri] ?? []);
    }

    /**
     * @param array $attributes
     * @return array
     */
    private function prepareAttributes(array $attributes): array
    {
        $allowedKeys = ['success', 'log_id', 'message'];
        $filteredParameters = \array_filter(
            $attributes,
            static function ($key) use ($allowedKeys) {
                return \in_array($key, $allowedKeys, true);
            },
            ARRAY_FILTER_USE_KEY
        );

        return \array_merge($attributes['pagination'] ?? [], $filteredParameters);
    }
}
