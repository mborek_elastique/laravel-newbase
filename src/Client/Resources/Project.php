<?php

namespace Wocozon\Newbase\Client\Resources;

use Wocozon\Newbase\Client\Contracts\ModelResourceInterface;
use Wocozon\Newbase\Client\Contracts\ReferenceInterface;
use Wocozon\Newbase\Client\Query\Builder;
use Wocozon\Newbase\Client\Resources\Traits\HasReadModelOperation;
use Wocozon\Newbase\Client\Resources\Traits\HasReferenceField;
use Wocozon\Newbase\Client\Resources\Traits\HasWriteModelOperations;

/**
 * Class Project
 * @package Wocozon\Newbase\Client\Resources
 */
class Project extends Resource implements ModelResourceInterface, ReferenceInterface
{
    use HasReadModelOperation;
    use HasWriteModelOperations {
        applyModelQuery as defaultModelQuery;
    }

    use HasReferenceField;

    public const PROGRAM = 'CustomerProject';

    /**
     * @var int
     */
    protected int $perPage = 10;

    /** @var array|string[] */
    protected array $tables = [
        'project',
        'project_line',
    ];

    /** @var string */
    protected string $refField = 'name';

    /**
     * @return Builder
     */
    public function makeQuery(): Builder
    {
        $builder = parent::makeQuery();
        $builder->addStatement('type_program', 'eq', self::PROGRAM);
        return $builder;
    }

    public function applyModelQuery(array $query): array
    {
        return $this->defaultModelQuery(
            \array_merge(
                [
                    'program' => self::PROGRAM,
                ],
                $query,
            )
        );
    }
}
